Demo site
=========

Simple website for searching a set of articles labelled with topics.

Data
----

Data is assumed to be a liste of articles in json format with a field 'topic-pred' containing a list of topics.
Example:

```
{
  'title': 'Title of the article',
  'abstract': 'Abstract of the article',
  'authors': 'List of authors as a string',
  'publication_date': '2020-05-28',
  'url': 'https://url-to-full-text',
  'topic-pred': [ 'topic1', 'topic2', ...]
}
```

The included search engine is a very simple search engine implementing BM25 ranking after stopword removal.

Running
-------

```
npm install
node create-db.js papers-with-topics.json
npm start
```
