"use strict"
var express = require('express');
var mustache = require('mustache-express');

var model = require('./model');
var app = express();

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

app.use((req, res, next) => {
  res.locals.topic_menu = model.topics.map((topic) => ({name: topic}));
  return next();
});

app.get('/', (req, res) => {
  res.redirect('/topic/All');
});

app.get('/article/:id', (req, res) => {
  model.read(req.params.id, (err, doc) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.render('read', doc);
    }
  });
});

app.get('/topic/:topic', (req, res) => {
  res.locals.topic_menu = model.topics.map((topic) => ({name: topic, active: topic === req.params.topic}));
  model.for_topic(req.params.topic, req.query.page, (err, found) => {
    found.results = model.elidate(found.results);
    res.render('search', found);
  });
});

app.get('/search', (req, res) => {
  model.search(req.query.query, req.query.page, (err, found) => {
    found.results = model.elidate(found.results);
    res.render('search', found);
  });
});

//app.use(express.static('old/public'))

app.listen(8892, () => console.log('listening on http://localhost:8892'));
