const fs = require('fs');

var load = function(filename) {
  console.log('loading json...');
  const articles = JSON.parse(fs.readFileSync(filename));
  console.log(articles.length, 'articles');
  console.log('saving in db...');
  articles.sort((a, b) => -a.publication_date.localeCompare(b.publication_date));

  const index = {};
  var id = 0;
  for(var article of articles) {
    article._id = id;
    for(var topic of article['topic-pred']) {
      if(!(topic in index)) index[topic] = [];
      index[topic].push(id);
    }
    id ++;
  }

  for(var topic in index) {
    index[topic].sort((a, b) => -articles[a].publication_date.localeCompare(articles[b].publication_date));
  }

  var data = {
    index: index,
    articles: articles,
  };

  fs.writeFileSync('data.json', JSON.stringify(data));
  console.log('indexing...');
  const search_engine = require('./search');
  search_engine.build_index(articles.map((doc) => doc.title + ' ' + doc.abstract));
  search_engine.save_index('data.bm25');
}

if(process.argv.length < 3) {
  console.log('usage: node create-db.js <json-data-path>');
} else {
  load(process.argv[2]);
}

