"use strict"
const fs = require('fs');

const {index, articles} = JSON.parse(fs.readFileSync('data.json').toString());

exports.read = (id, callback) => {
  callback(null, articles[id]);
};

exports.for_topic = (topic, page, callback) => {
  const num_per_page = 36;
  topic = topic || 'All';
  page = parseInt(page || 1);

  var query = {'topic-pred': topic};
  var results, num_found;
  if(topic === 'All') {
    results = articles.slice((page - 1) * num_per_page, page * num_per_page);
    num_found = articles.length;
  } else if(topic in index) {
    results = index[topic].slice((page - 1) * num_per_page, page * num_per_page).map((id) => articles[id]);
    num_found = index[topic].length;
  } else {
    results = [];
    num_found = 0;
  }
  callback(null, {
        results: results,
        num_found: num_found, 
        next_page: page + 1,
        topic: topic,
        page: page,
        num_pages: parseInt(num_found / num_per_page) + 1,
      });
}

const search_engine = require('./search');
search_engine.load_index('data.bm25');

exports.search = (query, page, callback) => {
  const num_per_page = 32;
  query = query || "";
  page = parseInt(page || 1);

  const found = search_engine.search(query);
  const num_found = found.length;
  const results = found.slice((page - 1) * num_per_page, page * num_per_page).map(result => articles[result[0]]);
  callback(null, {
      results: results,
      num_found: num_found, 
      query: query,
      next_page: page + 1,
      page: page,
      num_pages: parseInt(num_found / num_per_page) + 1,
    });
};

exports.topics = ['All', 'Prevention', 'Diagnosis', 'Treatment', 'Case Report', 'Mechanism', 'Transmission', 'Epidemic Forecasting', 'General Info'];

exports.elidate = (articles) => {
  articles = JSON.parse(JSON.stringify(articles)); // deep copy
  for(var i = 0; i < articles.length; i++) {
    var article = articles[i];
    var authors = article.authors.split(';');
    if(authors.length > 3) {
      article['authors'] = authors.slice(0, 3).join(';') + ' et al.'; 
    }
    if(article.abstract.length > 200) article.abstract = article.abstract.substr(0, 200) + '...';
  }
  return articles;
}
